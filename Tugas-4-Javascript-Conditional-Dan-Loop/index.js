// soal 1
var nilai = 54;

if(nilai >= 85 && nilai <= 100){
    console.log("A");
}else if(nilai <85 && nilai >= 75){
    console.log("B");
}else if(nilai <75 && nilai >= 65){
    console.log("C");
}else if (nilai <65 && nilai >=55){
    console.log("D");
}else if(nilai <55){
    console.log("E");
}

// soal 2
var tanggal = 8;
var bulan = 5;
var tahun = 2002;

switch(bulan){
    case 1:{console.log( tanggal +" "+ "Januari"+ " " +tahun);break;}
    case 2:{console.log( tanggal +" "+ "Februari"+ " " +tahun);break;}
    case 3:{console.log( tanggal +" "+ "Maret"+ " " +tahun);break;}
    case 4:{console.log( tanggal +" "+ "April"+ " " +tahun);break;}
    case 5:{console.log( tanggal +" "+ "Mei"+ " " +tahun);break;}
    case 6:{console.log( tanggal +" "+ "Juni"+ " " +tahun);break;}
    case 7:{console.log( tanggal +" "+ "Juli"+ " " +tahun);break;}
    case 8:{console.log( tanggal +" "+ "Agustus"+ " " +tahun);break;}
    case 9:{console.log( tanggal +" "+ "September"+ " " +tahun);break;}
    case 10:{console.log( tanggal +" "+ "Oktober"+ " " +tahun);break;}
    case 11:{console.log( tanggal +" "+ "November"+ " " +tahun);break;}
    case 12:{console.log( tanggal +" "+ "Desember"+ " " +tahun);break;}
    default : {console.log("input bulan salah")}

}
// soal 3

var n = 7;
var  string = "";
for (let i = 1; i <= n; i++) {
  for (let j = 0; j < i; j++) {
    string += "#";
  }
  string += "\n";
}
console.log(string);

//soal 4
var m = 10;
var strip = "===";


for (let i = 1; i <= m; i++) {

    if (i == 1 || i % 2 >= 0 && i % 3 == 1) {
        console.log(`${i} - I love programming`);
    }
    else if (i % 2 >= 0 && i % 3 == 2) {
        console.log(`${i} - I love Javascript`);
    }
    else if (i % 3 == 0) {
        console.log(`${i} - I love VueJS`);
        console.log(strip)
          
    }
}