//soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var satu = pertama.substring(0,4);
var dua = pertama.substring(11,18);
var tiga = kedua.substring(0,7);
var empat = kedua.substring(7,18)
console.log(satu+ " " + dua+" "+ tiga+ " "+ empat.toUpperCase());

//soal 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var num1 = parseInt(kataPertama);
var num2 = parseInt(kataKedua);
var num3 = parseInt(kataKetiga);
var num4 = parseInt(kataKeempat);

console.log((num2+num3)*(num1-num4));

//soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);